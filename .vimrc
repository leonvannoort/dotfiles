" Use vim settings instead of vi
set nocompatible
nnoremap <M-[> :bp<cr>

so ~/.vim/plugins.vim


"------------------------- SETTINGS -------------------------------------------
"
let mapleader = ','                     " Chande <Leader> to ,
syntax enable
set backspace=indent,eol,start          " proper behaviour for backspace
set clipboard=unnamed                   " we can copy/paste from os clipboard
set autowriteall                        " write files before changing buffers
set t_CO=256                            " let terminal know we have 256 colors
set t_ut=                               " 256-colors for vim in tmux
set timeoutlen=1000
set ttimeoutlen=5
"
"---------- Lets turn off some stuff -------------
set noshowmode
set noruler
set noshowcmd
set noerrorbells visualbell t_vb=       " turn errorbells off.
"
"---------- Linenumbers --------------------------
set number                              " turn linenumbers on
set numberwidth=5
set rnu                                 " I want relative numbering
"
"---------- Tabs ---------------------------------
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
"
"---------- Scrolling behaviour -----------------
set scrolloff=8                         " start scrolling when close to window edge
set sidescrolloff=15
set sidescroll=1
"
"---------- Wildmenu -----------------------------
" invoke by typing first characters of terminal command
" and press <tab> and again <tab>
set wildmenu
set wildmode=list:longest,full
"
"---------- Visual marker for columnwidth --------
set textwidth=100
set wrapmargin=0
set colorcolumn=+1
set formatoptions=cqrn1
"
"---------- Search options -----------------------
set hlsearch                            " highlight results
set ignorecase                          " use case insensitive search
set smartcase
"
"---------- Splits -------------------------------
" Get rid of ugly vertical split border
set fillchars+=vert:\       "last character is a space
set foldcolumn=1
" Set default split position
set splitright
set splitbelow
" Set dimensions splits
set winwidth=104
set winheight=5
set winminheight=5
set winheight=999
"
"---------- Theming ------------------------------
colorscheme codedark
"
"
"------------------------- Custom Keybindings ----------------------------------
"
"---------- General ------------------------------
"
" Normal behaviour for up and down on wrapped lines
nnoremap j gj
nnoremap k gk
"
" Escape out of Insert Mode
inoremap jj <Esc>
"
" jump up or down from within a line, without wrapping the line 
inoremap <C-j> <Esc>o
inoremap <C-k> <Esc>O
"
" Turn of highlighted search results
nnoremap <silent><Leader><space> :noh<CR>
" Quickly browse to any tag/symbol in the project.
" run ctags −R to regenerated the index.
nmap <Leader>f :tag<space>
" 
" Insert a blank line, without entering insert mode
nnoremap \<CR> o<Esc>
" 
" When I forget to lift my finger from shift on saving or quiting
command! W w
command! Q q
" 
"
"--------- Move around Buffers -------------------
"
" Toggle between two buffers
nnoremap <Leader><Leader> <C-^> 
"
" Keybindings for previous and next
nnoremap [b :bp<cr>
nnoremap ]b :bn<cr>
"
" Keybinding for close buffer
nnoremap <Leader>x :bd<cr>
"
"---------- Splits -------------------------------
nnoremap <silent> <Right> :vertical resize +5<cr>
nnoremap <silent> <Left> :vertical resize -5<cr>
"
" Easier mappings for moving around in splits
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l
"
"---------- Brackets  ----------------------------
" When typing two brackets, the cursor jumps inside.
inoremap () ()<Esc>i
inoremap (); ();<Esc>hi
inoremap {} {<CR>}<Esc>O
inoremap [] []<Esc>i
inoremap []; [];<Esc>hi
inoremap <> <><Esc>i
inoremap '' ''<Esc>i
inoremap ''; '';<Esc>hi
inoremap "" ""<Esc>i
inoremap ""; "";<Esc>hi
"
" Jump outside brackets
inoremap <C-l> <Esc>la
"
"
"---------- Easy Access to dotfiles --------------
nnoremap <Leader>ev :e ~/.vimrc<cr>
nnoremap <Leader>plug :e ~/.vim/plugins.vim<cr>
"
"
"
"------------------------- Plugins ---------------------------------------------
"
"---------- Airline -----------------------------
let g:airline_theme='codedark'
let g:airline_powerline_fonts=1
" 
"---------- YouCompleteMe ------------------------
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
"
"---------- Ultisnips ----------------------------
let g:SuperTabDefaultCompletionType = '<C-n>'
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
set completeopt-=preview
"
"---------- Nerdtree -----------------------------
let NERDTreeHijackNetrw=0
let NERDTreeShowHidden=1
nnoremap <Leader>1 :NERDTreeToggle<cr>
"
"---------- Syntastic ----------------------------
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_mode_map = { 'mode': 'passive' }
let g:syntastic_php_checkers = ['php', 'phpcs']
nmap <Leader>st :SyntasticToggleMode<cr>
"
"---------- PHP-CS-Fixer -------------------------
let g:php_cs_fixer_level = "psr2"
let g:php_cs_fixer_verbose = 1
let g:php_cs_fixer_php_path = "/usr/local/bin/php"
nnoremap <silent><leader>pf :call PhpCsFixerFixFile()<CR>
"
"---------- PDV Documentor ------------------------ 
"
let g:pdv_template_dir = $HOME ."/.vim/bundle/pdv/templates_snip"
nnoremap <leader>d :call pdv#DocumentWithSnip()<CR>
"
"---------- Vim-PHP-Namespace --------------------
" Needs ctags
function! IPhpInsertUse()
    call PhpInsertUse()
    call feedkeys('a',  'n')
endfunction
autocmd FileType php inoremap <Leader>n <Esc>:call IPhpInsertUse()<CR>
autocmd FileType php noremap <Leader>n :call PhpInsertUse()<CR>

function! IPhpExpandClass()
    call PhpExpandClass()
    call feedkeys('a', 'n')
endfunction
autocmd FileType php inoremap <Leader>nf <Esc>:call IPhpExpandClass()<CR>
autocmd FileType php noremap <Leader>nf :call PhpExpandClass()<CR>
"
"---------- Keep relative numbering on -----------
function! ToggleNumbersOn()
    set nu!
    set rnu
endfunction

function! ToggleRelativeOn()
    set rnu!
    set nu
endfunction

autocmd FocusLost * call ToggleNumbersOn()
autocmd FocusGained * call ToggleRelativeOn()
autocmd InsertEnter * call ToggleRelativeOn()
autocmd InsertLeave * call ToggleRelativeOn()
"
"--------- Autosource vimrc on save --------------
augroup autosourcing
    autocmd!
        autocmd BufWritePost .vimrc source %
augroup END
"
"
"
"---------- Disable Keys -------------------------------------------------------
"
" In order to get used to new keybindings remove, we disable old keybindings
"
inoremap <Up> <NOP>
inoremap <Down> <NOP>
inoremap <Left> <NOP>
inoremap <Right> <NOP>
inoremap <Esc> <NOP>
