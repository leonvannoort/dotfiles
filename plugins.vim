filetype plugin indent on " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

Plugin 'jiangmiao/auto-pairs'
Plugin 'christoomey/vim-tmux-runner'
Plugin 'christoomey/vim-tmux-navigator'
" Theming
Plugin 'vim-airline/vim-airline'
Plugin 'tomasiser/vim-code-dark'

" FileMenu
Plugin 'tpope/vim-vinegar'          "Simple menu invoked by pressing -
Plugin 'scrooloose/nerdtree'

Plugin 'ctrlpvim/ctrlp.vim'
" Autocomplete and snippets -> need supertab
Plugin 'ervandew/supertab'
Plugin 'valloric/youcompleteme'
Plugin 'sirver/ultisnips'

" Editing tools
Plugin 'scrooloose/nerdcommenter'
Plugin 'tpope/vim-surround'

"---------- Tools --------------------------------
" Git
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'

"---------- PHP Specific -------------------------
Plugin 'StanAngeloff/php.vim'       " PHP-syntax highlighting
"Plugin 'alvan/vim-php-manual'
Plugin 'arnaud-lb/vim-php-namespace'
Plugin 'stephpy/vim-php-cs-fixer'   " Fix to PSR standard
Plugin 'scrooloose/syntastic'       " Check PSR
" Docblocks generator
Plugin 'tobyS/vmustache'            " dependency
Plugin 'tobyS/pdv'                  " also requires ultisnips
Plugin 'adoy/vim-php-refactoring-toolbox'

Plugin 'rking/ag.vim'
Plugin 'skwp/greplace.vim'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
"" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

